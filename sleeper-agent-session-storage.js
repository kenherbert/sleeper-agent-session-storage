/*
 * Sleeper-agent handler to write to session storage.
 */

 if(window.sessionStorage && sleeper) {
    'use strict';

    var sessionStorageHandler = (function() {
        var keyPrefix = 'sleeper-';

        var _clearAll = function() {
            var i;

            for(i in sessionStorage) {
                if(i.substr(0, keyPrefix.length) === keyPrefix) {
                    sessionStorage.removeItem(i);
                }
            }
        };

        var _getFormattedArgs = function(args) {
            return JSON.stringify(args);
        };

        var _log = function(args) {
            _writeToStorage('log', args);
        };

        var _dir = function(args) {
            _writeToStorage('dir', args);
        };

        var _error = function(args) {
            _writeToStorage('error', args);
        };

        var _warn = function(args) {
            _writeToStorage('warn', args);
        };

        var _writeToStorage = function(type, args) {
            var date = new Date();
            var key = keyPrefix + type + '-' + date.getTime();
            var item = 'Time: ' + date.toLocaleString() + ' ----- Data: ' + _getFormattedArgs(args);

            sessionStorage.setItem(key, item);
        };

        _clearAll();

        return {
            log: _log,
            dir: _dir,
            error: _error,
            warn: _warn
        };
    }());

    sleeper.addStream('sessionStorage', sessionStorageHandler);
}
