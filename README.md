# Sleeper-agent-session-storage
A session storage stream for [sleeper-agent](https://github.com/kapilkaisare/sleeper-agent)

This little script adds an alternative stream to sleeper-agent's output.


## Why session storage?
I had need of a session-persistent logger, instead of the usual page-load persistence of the console.

It makes for an easier way to debug the natural flow of things when a refresh/redirect is expected.



## Limitations and gotchas
It currently only supports logging of messages that can be successfully passed to `JSON.stringify()`.

When it is instantiated all session storage entries with the same prefix (default `sleeper-`) will be removed. There typically shouldn't be any conflicts here, but it is noted just in case.



## Dependencies
Just sleeper-agent. That's it.



## Browser support
It should support all browsers that sleeper-agent supports, but even if it is loaded in an unsupported browser it should just die gracefully.



## Future plans
None really. It was just a one-off thing I needed a few years ago, and have only just decided to clean it up and release it.

Maybe, maybe, just maybe I will implement multiple prefix handling in a single instance. But probably not.